# VMI-UI README
This package contains the source code for CCTVal's ventilator software.
To see a detailed description, check out the [**Documentation**](#documentation) section.
The package comes with a proprietary license, so the most likely scenario is that you shouldn't be
reading this.

If you are in the project, welcome!
If you have any issues with this README or the code feel free to contact me on the provided email.
If for any reason I am no longer affiliated to the university and you have any issues with or questions about the provided code, then you can contact me under my personal email at `bruno.benkel@gmail.com`.

    Copyright (C) CCTVal - All Rights Reserved
    Unauthorized copying of this file, via any medium is strictly prohibited
    Proprietary and confidential
    Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

---
## Table of Contents
1. [**Setup**](#setup)
    * [Ubuntu 20.10](#ubuntu20)
        * [Qt Setup](#"ub20_qt")
        * [Anaconda](#"ub20_anaconda")
        * [Libraries](#"ub20_libs")
        * [Verification](#"ub20_verification")
    * [Raspberry Pi](#raspberry)
        * [Python Dependencies](#rasp_pythondeps)
        * [Verification](#rasp_verification)
2. [**Running**](#running)
3. [**Documentation**](#documentation)

---
## Setup <a name="setup"></a>
### Ubuntu 20.10 <a name="ubuntu20"></a>
All this setup was tested in an Ubuntu 20.10 (Groovy), but since the software is Python-based probably any Linux system can run it a-ok.

##### Qt Setup <a name="ub20_qt"></a>
Update your repository list and install the extended dependencies needed for Qt.
Qt is the library used to handle the UI.
```sh
sudo apt update; sudo apt install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
```

While not strictly necessary, `qtcreator` can be installed to facilitate the editing of `.ui` files.

You can either download the source file from [**here**](https://download.qt.io/official_releases/qtcreator/4.12/4.12.1/qt-creator-opensource-linux-x86_64-4.12.1.run) or `wget` it:
```sh
wget https://download.qt.io/official_releases/qtcreator/4.12/4.12.1/qt-creator-opensource-linux-x86_64-4.12.1.run
```

and then run:
```sh
sudo chmod +x ./qt-creator-opensource-linux-x86_64-4.12.1.run
./qt-creator-opensource-linux-x86_64-4.12.1.run
```

Follow the installer's instructions and then run Qt Creator to verify the installation went fine.

##### Anaconda <a name="ub20_anaconda"></a>
While technically not needed, we recommend you use Anaconda to avoid messing up your local environment.
If this is not a concern and you don't wanna go through the hassle of using anaconda, you can safely skip this part and install everything using pip instead of conda.

These instructions were taken from [the official anaconda documentation](https://docs.anaconda.com/anaconda/install/linux/).

Then download the Anaconda installer from [here](https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh) or by running:
```sh
wget https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh
```

Then install Anaconda by running
```sh
sudo chmod +x ./Anaconda3-2020.02-Linux-x86_64.sh
./Anaconda3-2020.02-Linux-x86_64.sh
```

Read and accept the license terms, and then choose the installation directory.
If for some deranged reason you want anaconda to be started automatically each time you open a terminal, select `yes` in the `anaconda init` option.

Finally, create an anaconda virtual environment:
```sh
conda create -n [env name] python=3.7.7
conda activate [env name]
```

There is technically no particular reason to use Python 3.7.7 precisely, but it aids to keep everything nice and consistent.

##### Libraries <a name="ub20_libs"></a>
Some critical libraries are required to get everything running.

Pyside2 is used to handle the connection between Python and Qt.
PyQTGraph handles the plots in the UI itself.
PySerial manages the serial communication between the arduinos and the Python software.

Install it by running:
```sh
conda install -c conda-forge pyside2 pyqtgraph pyserial
```

##### Verification <a name="ub20_verification"></a>
To assert that everything was installed correctly, start up a python terminal and input
```python
import PySide2
import numpy as np
import pyqtgraph as pg
import serial

pg.systemInfo()
```

Assuming that the python shell doesn't throw any errors, the output should be
```
qt bindings: PySide2 5.13.2 Qt 5.12.5
```

---
### Raspberry Pi <a name="raspberry"></a>
##### Python Dependencies <a name="rasp_pythondeps"></a>
This project runs over Raspbian OS.
To run, make sure your cache is up to date
```sh
sudo apt update
```

Then install the necessary python 3.7 dependencies
```sh
sudo python3.7 -m pip install numpy pyserial pipenv
```

In contrast with Ubuntu, we'll run the code over pipenv instead of conda.
The reason for this eludes me, and if you manage to get things running with conda let me know and I'll update the README.

Create a directory for the environment and run the pipenv shell:
```sh
mkdir UI-QT
cd UI-QT
pipenv shell
exit
```

After Pipenv is correctly installed, run
```sh
sudo apt-get install libatlas3-base git python3-pyside2*
```

And copy PySide2 into the environment directory:
```sh
mkdir ~/.local/share/virtualenvs/UI-QT-<Hash>/lib/python3.7/site-packages/PySide2/
cd /usr/lib/python3/dist-packages/PySide2
cp -R ./* ~/.local/share/virtualenvs/UI-QT-<Hash>/lib/python3.7/site-packages/PySide2/
```
The `<Hash>` was defined during Pipenv's installation, and should be easy to figure out.

Installing PyQtGraph is a tiny bit trickier in the Raspy, since it has to be installed from source.
```sh
cd /path/to/UI-QT
pipenv shell
git clone https://github.com/pyqtgraph/pyqtgraph
cd pyqtgraph
python3.7 setup.py install
```

##### Verification <a name="rasp_verification"></a>
With the environment activated, test the correct installation of the dependencies by running this in a Python shell
```python
import PySide2
import numpy as np
import pyqtgraph as pg
import serial

pg.systemInfo()
```

Assuming that there aren't any errors, the output should be
```
qt bindings: PySide2 5.11.2 Qt 5.11.3
```

---
## Running <a name="running"></a>
Now that all the dependencies are installed, it is time to clone the repository.
Go into your selected location and run
```sh
git clone https://gitlab.com/cctval-iot/ui-vmi.git
cd ui-vmi
```

Before running the application itself you need to copy the fonts to your system
```sh
sudo cp -r ./assets/fonts /usr/share/fonts/truetype
```

Then activate the environment:
```sh
conda activate [env name]
```

It is common (and good!) that the connected arduinos are not automatically accessible if you're running this in your Linux machine.
To enable access to them you can run the pre-made script
```sh
sudo ./entable_ttyACM.sh
```

Finally, you can run the application via the `start.sh` script:
```sh
./start.sh
```

---
## Documentation <a name="documentation"></a>
The [Sphinx](href=https://www.sphinx-doc.org/en/master/index.html) package handles this project's documentation.
To install sphinx, run:
```sh
conda install Sphinx
```

Then generate the documentation by running:
```sh
sphinx-build -b html sphinx-src/ sphinx-build/
```

And simply open `sphinx-build/index.html` in your favorite browser to read the generated documentation fresh out of the oven.
