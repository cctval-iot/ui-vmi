# Small script to simplify your life and enable the connected arduinos.
ttyACMs=$(find /dev/ -maxdepth 1 -name "ttyACM*")
for ttyACM in $ttyACMs; do chmod 0777 $ttyACM; done
