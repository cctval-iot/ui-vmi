Connections
==================================

Connection files, as their name suggests, handle the connections with the blender and the sensor
arduinos. These are handled via a parent class (`DeviceConnection`).

.. automodule:: connection.DeviceConnection
    :members:
