Controllers
==================================

Controllers handle the back-end of the UI. This includes the general setup, the UI itself, each
particular device, the horometer, and storage. The `Controller.py` file works as a main file to
the application, handling the back-end of the main ventilator UI and setting up each of the UI's
component and their network of communication.

.. automodule:: controller.BlenderController
    :members:
.. automodule:: controller.Controller
    :members:
.. automodule:: controller.DeviceController
    :members:
.. automodule:: controller.HorometerController
    :members:
.. automodule:: controller.SensorController
    :members:
.. automodule:: controller.UIController
    :members:
