State
==================================

State files store the internal state of the entire machine, used to reduce the number of requests
sent to the devices. The module itself doesn't act on the data, focusing only on storing and
managing it.

.. automodule:: state.BlenderState
    :members:
.. automodule:: state.Constants
    :members:
.. automodule:: state.DeviceState
    :members:
.. automodule:: state.SensorState
    :members:
