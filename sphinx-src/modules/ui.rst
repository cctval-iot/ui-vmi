UI
==================================

UI files handle the different views for the UI-VMI package. Each view is handled by a python code
file and a QT UI file, which follow a standard naming convention:

* QT UI file: `filename.ui`
* Python code file: `filenameUI.py`

.. automodule:: ui.AlarmConfigUI
    :members:
.. automodule:: ui.CalibUI
    :members:
.. automodule:: ui.ConfigUI
    :members:
.. automodule:: ui.ConfirmShutdownUI
    :members:
.. automodule:: ui.EngModeUI
    :members:
.. automodule:: ui.PopAlarmUI
    :members:
.. automodule:: ui.UIElement
    :members:
.. automodule:: ui.VentUI
    :members:
