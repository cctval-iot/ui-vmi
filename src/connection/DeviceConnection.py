# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

import serial
import serial.tools.list_ports
import threading
import time

from PySide2.QtCore import QObject

class DeviceConnection(QObject):
    """
    Parent class to BlenderConnection and SensorConnection. Defines how the serial connections to
    the blender and the sensor are to be handled. Its responsibilities include:

    * Handling the opening and closing of a connection to the associated device.
    * Reading data from a connected device and returning it when requested.
    * Writing data to a connected device.
    """

    def __init__(self, devname, constants):
        super(DeviceConnection, self).__init__()

        self.devName    = devname
        self.arduinoVID = constants.ARDUINOVID
        self.pid        = None
        self.params     = None
        self.signals    = None
        self.okPrefix   = None
        self.loopRange  = constants.LOOPRANGE
        self.loopSleep  = constants.LOOPSLEEP
        if self.devName == constants.B_DEVNAME:
            self.pid      = constants.B_PID
            self.params   = constants.B_PARAMS
            self.signals  = constants.B_SGNL
            self.okPrefix = constants.B_OKPRFX
        elif self.devName == constants.S_DEVNAME:
            self.pid      = constants.S_PID
            self.params   = constants.S_PARAMS
            self.signals  = constants.S_SGNL
            self.okPrefix = constants.S_OKPRFX

        self.serialConnection = serial.Serial(self.getDev(), constants.BAUDRATE,
                timeout=constants.TIMEOUT)
        self.serialConnection.flush()

        self.dataRecv = [b''] # Data received on each call. Initially empty.
        time.sleep(2) # Leave the arduinos some time to rest.

    # === CONNECT ==================================================================================
    def getDev(self):
        """
        Get port to a connected device in order to establish a connection. This function handles
        only variables from the class and thus requires no IO.
        """
        # NOTE: This should print an error and exit when the program doesn't have access rights to
        #       the arduino. Currently it just throws an exception and dies.
        devs = []
        for port in serial.tools.list_ports.comports():
            if port.vid == self.arduinoVID and port.pid == self.pid:
                return port.device
        print("[%s] Error fatal: Dispositivo no está conectado." % (self.devName))
        exit(1)

    def flush(self):
        """Flush the input and output from the device.
        """
        time.sleep(2)
        self.serialConnection.flushInput()
        self.serialConnection.flushOutput()

    # def closeConnection(self):
    #     """
    #     End the connection with the device. This is currently unused, but might be useful if more
    #     refined functionalities are to be implemented in the future.
    #     """
    #     self.serialConnection.close()

    # === READ =====================================================================================
    def read(self):
        """
        Read data received from a connected device, assuming its a String, and separate it according
        to the standard set by the team. This standard requires each parameter in the string to be
        separated by a comma.
        The data is saved in the class' parameter dataRecv.
        """
        # NOTE: The current standard assumes that the last parameter in the string is the active
        # alarm. If this standard is at any time changed, this function needs to be updated
        # alongside it to continue working.
        self.dataRecv = self.serialConnection.readline().rstrip().split(b',')

    def getDataReceived(self):
        """Return the last received data.
        """
        return self.dataRecv

    # === WRITE ====================================================================================
    def encodeMsg(self, id, val=''):
        """
        Encode outgoing messages. Kinda hard to understand, but its only used by this class so I
        guess that's fine.
        """
        # NOTE: This should be simplified.
        if type(val) != str:
            # Values containing '/' are excluded and replaced for the next character.
            if (val.to_bytes(2,'big')[0] == 47 or val.to_bytes(2,'big')[1] == 47): val = val+1
            return (id+val.to_bytes(2,'big').decode('latin_1')+'/').encode('charmap')
        else:
            return (id+val+'/').encode("charmap")

    def sendParamWorker(self, id, val, block):
        """
        Worker used to send parameters to an arduino. As its name suggests, this method should only
        be called by a thread created specificly to it, since it contains infinite loops and sleeps.
        """
        okCode = (self.okPrefix + id).encode() # OK message to wait for.
        msgIn = ""
        msgOut = self.encodeMsg(id, val)
        while msgIn != okCode: # Blocking call.
            self.write(msgOut)
            if block:
                self.read()
            for i in range(self.loopRange):
                msgIn = self.getDataReceived()[0]
                if msgIn == okCode:
                    break
                time.sleep(self.loopSleep)

    def sendParam(self, id, val, block=False):
        """
        Send a parameter to an arduino via a new thread, invoking sendParamWorder in the process.
        Returns False if succesful, and True if an error was encountered.
        """
        if val != '': val = int(val)
        if ((id not in self.params) and (id not in self.signals)):
            raise ValueError("Incorrect value")
            return True
        if not block:
            th = threading.Thread(target=self.sendParamWorker, args=(id, val, block))
            th.start()
        else:
            self.sendParamWorker(id, val, block)
        return False

    def write(self, msg):
        """Write data to the connected device. Essentially just calls the serial function.
        """
        self.serialConnection.write(msg)
