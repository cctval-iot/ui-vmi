# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from controller.DeviceController import DeviceController

class BlenderController(DeviceController):
    """Device controller associated to the blender. More information is available in parent class.
    """
    def __init__(self, constants, blenderState, blenderConnection):
        super(BlenderController, self).__init__(constants, blenderState, blenderConnection)
        self.devName   = "Blender"
        self.msgPrefix = b"BL"
        self.meass     = self.constants.B_MEASS
