# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from PySide2.QtWidgets import QWidget, QStackedWidget, QHBoxLayout
from PySide2.QtCore import QTimer, QThread

from state import Constants, BlenderState, SensorState
from connection import DeviceConnection
from controller import BlenderController, HorometerController, SensorController, UIController
from ui import VentUI, EngModeUI, ConfigUI, CalibUI, AlarmConfigUI, PopAlarmUI, ConfirmShutdownUI

class Controller(QWidget):
    """
    Main class for the whole ventilator program. This involves:

    * Initiating the program, initializing classes, connections between classes, views, etc.
    * Connecting the QT signals and slots (https://doc.qt.io/qt-5/signalsandslots.html).
    * Start and stop the different timers (serial connections, UI, and alarms).
    * Display the current time in the screen
    """
    def __init__(self):
        super(Controller, self).__init__()

        # === CLASSES INITIALIZATION ===============================================================
        # states.
        self.constants = Constants.Constants()
        self.bState    = BlenderState.BlenderState(self.constants)
        self.sState    = SensorState.SensorState(self.constants)

        # connections.
        self.bConn   = DeviceConnection.DeviceConnection(self.constants.B_DEVNAME, self.constants)
        self.bThread = QThread() # Start a new thread dedicated to reading from the blender.
        self.bThread.start()
        self.bConn.moveToThread(self.bThread)

        self.sConn   = DeviceConnection.DeviceConnection(self.constants.S_DEVNAME, self.constants)
        self.sThread = QThread() # Start a new thread dedicated to the sensor's connection.
        self.sThread.start()
        self.sConn.moveToThread(self.sThread)

        # controllers.
        # NOTE: Currently, some of these classes write to their own associated json for storage.
        #       A standard JSONController class should be implemented at some point to standardize
        #       all these operations.
        self.bCtrl  = BlenderController.BlenderController(self.constants, self.bState, self.bConn)
        self.sCtrl  = SensorController.SensorController(self.constants, self.sState, self.sConn)
        self.hCtrl  = HorometerController.HorometerController(self.constants)
        self.uiCtrl = UIController.UIController(self.constants, self.bCtrl, self.sCtrl, self.hCtrl)

        # views.
        self.stack = QStackedWidget(self)
        self.setWindowTitle("VMI")
        self.showFullScreen()

        self.vUI    = VentUI.VentUI(self.uiCtrl)
        self.eUI    = EngModeUI.EngModeUI(self.uiCtrl)
        self.cfgUI  = ConfigUI.ConfigUI(self.uiCtrl, self.constants)
        self.calUI  = CalibUI.CalibUI(self.uiCtrl, self.constants)
        self.acgfUI = AlarmConfigUI.AlarmConfigUI(self.uiCtrl, self.constants)
        self.aUI    = PopAlarmUI.PopAlarmUI(self.uiCtrl, self.constants)
        self.cshUI  = ConfirmShutdownUI.ConfirmShutdownUI()

        # Connect UIController to UI elements.
        self.uiCtrl.connectToUI(self.vUI, self.eUI, self.calUI, self.aUI)

        # Add views to controller's stack.
        self.stack.addWidget(self.vUI)
        self.stack.addWidget(self.eUI)

        # Display main window.
        self.displayUI(0)

        # Set window parameters.
        hbox = QHBoxLayout(self)
        hbox.addWidget(self.stack)
        hbox.setContentsMargins(0, 0, 0, 0)
        self.setLayout(hbox)
        self.setGeometry(0, 0, 1031, 701)

        # === SIGNALS AND SLOTS CONFIGURATION ======================================================
        # VentUI
        self.vUI.ui.mod2_but.clicked.connect(self.power_onoff)
        self.vUI.ui.mod3_but.clicked.connect(self.vUI.setO2100)
        self.vUI.ui.but_ala.clicked.connect(self.acgfUI.show)
        self.vUI.ui.config_but.clicked.connect(self.cfgUI.show)
        self.vUI.ui.mod3_but_2.clicked.connect(self.vUI.askPlateauPressure)
        self.vUI.ui.eng_but.clicked.connect(lambda: self.displayUI(1))
        self.vUI.setStyleSheets()

        # ConfirmShutdownUI
        self.cshUI.ui.btn_enviar.clicked.connect(self.stop)
        self.cshUI.ui.btn_enviar.clicked.connect(self.cshUI.close)
        self.cshUI.ui.btn_cancelar.clicked.connect(self.cshUI.close)

        # PopAlarmUI
        self.aUI.ui.btn_silenciar.clicked.connect(self.aUI.mutePress)

        # EngModeUI
        self.eUI.ui.bck_but.clicked.connect(lambda: self.displayUI(0))
        self.eUI.ui.conf_but.clicked.connect(self.cfgUI.show)
        self.eUI.ui.calib_but.clicked.connect(self.calUI.show)
        self.eUI.setStyleSheets()

        # ConfigUI
        self.cfgUI.ui.bck_but.clicked.connect(self.cfgUI.close)
        self.cfgUI.ui.send_but.clicked.connect(self.cfgUI.updateCfg)
        self.cfgUI.setStyleSheets()

        # CalibUI
        self.calUI.ui.bck_but.clicked.connect(self.calUI.close)
        self.calUI.ui.control_desactivar.clicked.connect(self.calUI.deactivateControl)
        self.calUI.ui.send_but.clicked.connect(self.calUI.sendData)

        # AlarmConfigUI
        self.acgfUI.ui.bck_but.clicked.connect(self.acgfUI.close)
        self.acgfUI.ui.send_but.clicked.connect(self.acgfUI.sendData)

        if self.sState.getParam('E') != 0:
            self.forceStart()

    def displayUI(self,i):
        """Manage the Vent UI display.
        """
        if "resetPlots" in dir(self.stack.currentWidget()):
            self.stack.currentWidget().resetPlots()
        self.stack.setCurrentIndex(i)

    def power_onoff(self):
        """Start up or shut down all controllers.
        """
        if self.sState.getParam('E') == 0:
            self.bCtrl.start()
            self.sCtrl.start()
            self.hCtrl.start()
            self.uiCtrl.start()
            self.vUI.ui.mod2_but.setStyleSheet(
                    "QPushButton {" \
                        "background-color : rgb(204, 0, 0);" \
                        "border-style     : outset;" \
                        "color            : rgb(255, 255, 255);" \
                        "border-width     : 2px;" \
                        "border-radius    : 10px;" \
                        "padding          : 0px 10px 0px 10px;" \
                        "border-color     : beige;" \
                        "font             : 18pt Airbnb Cereal App Extra;}"
                    "QPushButton:pressed {background-color: rgb(78, 154, 6)}"
            )
            self.vUI.ui.mod2_but.setText("DETENER")
        else:
            self.cshUI.show()

    def forceStart(self):
        """
        Start up the raspberry in the scenario that the arduinos are already running. Implemented
        to add robustness to the system in the case that the raspberry crashes while the rest of the
        system remains running nominally.
        """
        print("UI is starting up, yet devices are already running. Adapting...")
        self.bCtrl.start()
        self.sCtrl.forceStart()
        self.hCtrl.start()
        self.uiCtrl.start()
        self.vUI.ui.mod2_but.setStyleSheet(
                "QPushButton {" \
                    "background-color : rgb(204, 0, 0);" \
                    "border-style     : outset;" \
                    "color            : rgb(255, 255, 255);" \
                    "border-width     : 2px;" \
                    "border-radius    : 10px;" \
                    "padding          : 0px 10px 0px 10px;" \
                    "border-color     : beige;" \
                    "font             : 18pt Airbnb Cereal App Extra;}"
                    "QPushButton:pressed {background-color: rgb(78, 154, 6)}"
        )
        self.vUI.ui.mod2_but.setText("DETENER")

    def stop(self):
        """
        Shut down all controllers. Some systems continue running after shutdown, and this is
        specified in their class descriptions.
        """
        if self.sState.getParam('E') != 0:
            self.bCtrl.stop()
            self.sCtrl.stop()
            self.hCtrl.stop()
            self.uiCtrl.stop()
            self.vUI.ui.mod2_but.setStyleSheet(
                    "QPushButton {" \
                        "background-color : rgb(78, 154, 6);" \
                        "border-style     : outset;" \
                        "color            : rgb(255, 255, 255);" \
                        "border-width     : 2px;" \
                        "border-radius    : 10px;" \
                        "padding          : 0px 10px 0px 10px;" \
                        "border-color     : beige;" \
                        "font             : 18pt Airbnb Cereal App Extra;}" \
                    "QPushButton:pressed {background-color: rgf(204, 0, 0)}"
            )
            self.vUI.ui.mod2_but.setText("INICIAR")

    def closeForm(self):
        """Shut down the entire application.
        """
        if self.sState.getParam('E') != 0:
            self.stop()
