# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from PySide2.QtCore import QObject, QTimer

class DeviceController(QObject):
    """
    Back-end handler for the configuration of the arduinos. This includes:

    * Reading devices configuration from internal state and from devices.
    * Return currently saved configuration when requested.
    * Setting parameters and returning them when requested.
    * Handling alarms-related parameters.
    """

    def __init__(self, constants, deviceState, deviceConnection):
        super(DeviceController, self).__init__()
        self.constants         = constants
        self.deviceState       = deviceState
        self.deviceConnection  = deviceConnection

        self.timer = QTimer(self) # QTimer for maintaining the internal state updated.
        self.timer.timeout.connect(self.deviceConnection.read)
        self.timer.timeout.connect(self.updateMeasurements)

    def start(self):
        """Start up the device controller's timer.
        """
        if not self.timer.isActive(): self.timer.start(20) # NOTE: Try with other intervals (100ms?)

    def stop(self):
        """Stop the device controller's timer.
        """
        if self.timer.isActive(): self.timer.stop()

    def updateMeasurements(self):
        """Update internal state with the measurements from the connection.
        """
        dataRecv = self.deviceConnection.getDataReceived()
        if dataRecv[0] == self.msgPrefix:
            for key in self.meass.keys():
                try:
                    self.deviceState.setMeas(key, float(dataRecv[self.meass[key]].decode("utf-8")))
                except UnicodeDecodeError as e:
                    print("UnicodeDecodeError: %s" % (e))
                except ValueError as e:
                    # NOTE: This exception is common when testing without the ventilator, so mute
                    #       it if you need to use the stdout.
                    print("ValueError: Tried to receive key %s, measurement obtained is %s."
                            % (key, dataRecv[self.meass[key]]))

    def checkAlarms(self):
        """Check if any alarms need to be raised, raising them when needed.
        """
        dataRecv = self.deviceConnection.getDataReceived()
        if dataRecv[0] != self.msgPrefix: return None

        msg = ""
        try:
            msg = dataRecv[-1].decode("utf-8")
        except UnicodeDecodeError as e:
            print("UnicodeDecodeError: %s" % (e))
            return None
        if msg == '0': return None # No alarm is up.

        # Remove muted alarms from list.
        alarms = [char for char in msg if not self.deviceState.checkAlarmMute(char)]
        if len(alarms) == 0: return None

        return alarms[0]

    def getAlarmDisplay(self, id):
        """Get alarm text from constants.
        """
        return self.deviceState.getAlarmDisplay(id)

    def muteAlarm(self, id):
        """Mute an alarm for MUTETIME seconds.
        """
        self.deviceState.muteAlarm(id)

    def getConfigParams(self):
        """Return configuration in the internal state per request.
        """
        return self.deviceState.getConfigParams()

    def getAlarmConfigParams(self):
        """Return alarm configuration in the internal state per request.
        """
        return self.deviceState.getAlarmConfigParams()

    def getParam(self, key):
        """Return currently saved parameter in internal state.
        """
        return self.deviceState.getParam(key)

    def setParams(self, paramDict):
        """
        Set a list of parameters given by paramDict to both devices and the internal state. Also
        saves into the associated json file.
        """
        for key in paramDict.keys():
            self.setParam(key, paramDict[key])
        self.deviceState.dump()

    def setParam(self, key, value):
        """
        Set parameter to device and internal state. If any error arises, return everything to how
        it was before. SHOULD ONLY be called by `setParams` in this same class.
        """
        oldValue = self.deviceState.getParam(key)
        if self.deviceState.setParam(key, value):
            print("Error: setParam falló, parámetro %s permanecerá igual." % (key))
            return
        if self.deviceConnection.sendParam(key, value, block=False):
            print("Error: setParam falló, parámetro %s permanecerá igual." % (key))
            self.deviceState.setParam(key, oldValue)

    def sendSignal(self, signal):
        """Send a signal to the device.
        """
        self.deviceConnection.sendParam(signal, '', block=False)

    def getMeas(self, key):
        """Return current measurement from internal state.
        """
        return self.deviceState.getMeas(key)
