# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

import json
import time # NOTE: This could also be implemented using QElapsedTimer.
from PySide2.QtCore import QObject

class HorometerController(QObject):
    """Back-end handler for the horometer. All time is handled by seconds, but printed in hours.
    """
    def __init__(self, constants):
        super(HorometerController, self).__init__()
        self.jsonFile  = constants.H_JSONPATH
        self.savedTime = self.getSavedTime()

    def getSavedTime(self):
        """Get time stored in the associated json file.
        """
        try:
            with open(self.jsonFile, 'r') as f:
                return json.loads(f.read())['T']
        except FileNotFoundError as e:
            print("File '%s' not found! Defaulting time to 0..." % (self.jsonFile))
            return 0
        except ValueError as e:
            print("JSONDecodeError: '%s'. Defaulting time to 0..." % (e))
            return 0

    def start(self):
        """Set initial time for the horometer.
        """
        self.savedTime = self.getSavedTime()
        self.initTime  = int(time.time())

    def stop(self):
        """Set finish time for the horometer.
        """
        elapsedTime = int(time.time()) - self.initTime
        try:
            with open(self.jsonFile, 'w') as f:
                json.dump({'T': self.savedTime + elapsedTime}, f, skipkeys=False, ensure_ascii=False)
        except ValueError as e:
            print("Error while dumping horometer time: '%s'. Skipping..." % (e))

    def getInitTime(self):
        """Get initial time from horometer in hours.
        """
        return int(self.savedTime/3600)

    def getTime(self):
        """Get current time from horometer in hours.
        """
        elapsedTime = int(time.time()) - self.initTime
        return int((self.savedTime + elapsedTime)/3600)
