# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from controller.DeviceController import DeviceController

class SensorController(DeviceController):
    """Device controller associated to the sensor. More information is available in parent class.
    """
    def __init__(self, constants, sensorState, sensorConnection):
        super(SensorController, self).__init__(constants, sensorState, sensorConnection)
        self.devName   = "Sensor"
        self.msgPrefix = b'd'
        self.meass     = self.constants.S_MEASS

    def start(self):
        """Start up the ventilator.
        """
        if self.deviceState.getParam('E') == 0:
            self.setParams({'E' : 1})
            # Send configuration to device when booting up.
            for param in self.constants.S_CFG:
                self.deviceConnection.sendParam(param, self.deviceState.params[param], block=False)
            super(SensorController, self).start()

    def forceStart(self):
        """
        Forcibly start up the ventilator. Used only in case the raspberry reboots for an arbitrary
        reason.
        """
        self.setParams({'E' : 1})
        # Send configuration to device to assert that what's displayed in the Raspberry is the same
        # as what's on the arduinos.
        for param in self.constants.S_CFG:
            self.deviceConnection.sendParam(param, self.deviceState.params[param], block=False)
        super(SensorController, self).start()

    def stop(self):
        """Shut down the ventilator.
        """
        if self.deviceState.getParam('E') != 0:
            self.setParams({'E' : 0})
            super(SensorController, self).stop()
