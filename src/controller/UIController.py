# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

import subprocess
import time # NOTE: This could also be implemented using QElapsedTimer.

from PySide2.QtCore import QObject, QTime, QTimer, SIGNAL
from PySide2.QtGui import QIcon

from .resources import *

class UIController(QObject):
    """
    Controller that manages all the views in the UI, communicating them with the blender and sensor
    controllers.
    """
    def __init__(self, constants, blenderController, sensorController, horometerController):
        super(UIController, self).__init__()

        # Class variables.
        self.upsStatus  = False # True if on, False if off.
        self.upsCounter = 0
        self.upsAlarm   = False # True if it should be up, False otherwise.
        self.aMute      = 0

        # Setup references.
        self.constants = constants
        self.bCtrl = blenderController
        self.sCtrl = sensorController
        self.hCtrl = horometerController

        # QTimer for updating the clock, battery display, and related pixmaps.
        clkTimer = QTimer(self)
        self.connect(clkTimer, SIGNAL("timeout()"), self.showTime)
        self.connect(clkTimer, SIGNAL("timeout()"), self.showStatus)
        self.connect(clkTimer, SIGNAL("timeout()"), self.upsCount)
        clkTimer.start(1000)

    def connectToUI(self, ventUI, engModeUI, calibUI, popAlarmUI):
        """Establish connection to UI elements and set up a timer to keep them updated.
        """
        self.vUI = ventUI
        self.eUI = engModeUI
        self.cUI = calibUI
        self.aUI = popAlarmUI

        self.showTime()
        self.showStatus()
        self.cUI.setInitTime()

        # QTimer that runs independent to the ventilator.
        self.passiveTimer = QTimer(self)
        self.passiveTimer.timeout.connect(self.aUI.update)
        self.passiveTimer.timeout.connect(self.vUI.update)
        self.passiveTimer.start(100)

        # QTimer that runs only when the ventilator is running.
        self.activeTimer = QTimer(self)
        self.activeTimer.timeout.connect(self.eUI.update)
        self.activeTimer.timeout.connect(self.cUI.update)

    def isOn(self):
        """Return True if the ventilator is on, False otherwise.
        """
        if self.sCtrl.getParam('E') != 0: return True
        else:                             return False

    def start(self):
        """Start up the controller's timer.
        """
        if not self.activeTimer.isActive(): self.activeTimer.start(100)

    def stop(self):
        """Stop the controller's timer.
        """
        if self.activeTimer.isActive(): self.activeTimer.stop()
        self.vUI.emptyLabels()
        self.vUI.resetPlots()

    def getParam(self, device, key):
        """Get a currently set parameter from the internal state.
        """
        if device == "Blender":
            return self.bCtrl.getParam(key)
        elif device == "Sensor":
            return self.sCtrl.getParam(key)
        else:
            print("Error: Device '%s' doesn't exist!" % (device))
            return None

    def getConfigParams(self, device):
        """Get the configuration parameters from a device.
        """
        if device == "Blender":
            return self.bCtrl.getConfigParams()
        elif device == "Sensor":
            return self.sCtrl.getConfigParams()
        else:
            print("Error: Device '%s' doesn't exist!" % (device))
            return None

    def getAlarmConfigParams(self, device):
        """Get the alarm configuration parameters from a device.
        """
        if device == "Blender":
            return self.bCtrl.getAlarmConfigParams()
        elif device == "Sensor":
            return self.sCtrl.getAlarmConfigParams()
        else:
            print("Error: '%s' doesn't exist!" % (device))
            return None

    def setParams(self, device, paramDict):
        """Set a set of parameters given by paramDict.
        """
        if device == "Blender":
            self.bCtrl.setParams(paramDict)
        elif device == "Sensor":
            self.sCtrl.setParams(paramDict)
        else:
            print("Error: '%s' doesn't exist!" % (device))

    def sendSignal(self, device, signal):
        """Send a signal to an arduino.
        """
        if device == "Blender":
            self.bCtrl.sendSignal(signal)
        elif device == "Sensor":
            self.sCtrl.sendSignal(signal)
        else:
            print("Error: '%s' doesn't exist!" % (device))

    def getMeas(self, device, key):
        """Get a measurement from the internal state.
        """
        if device == "Blender":
            return self.bCtrl.getMeas(key)
        elif device == "Sensor":
            return self.sCtrl.getMeas(key)
        else:
            print("Error: Device '%s' doesn't exist!" % (device))
            return None

    def getInitTime(self):
        """Get the saved time from the horometer.
        """
        return str(self.hCtrl.getInitTime())

    def getCurrTime(self):
        """Get the current time from the horometer.
        """
        return str(self.hCtrl.getTime())

    def checkAlarms(self):
        """Check if any alarms are up, returning the alarm's string per request.
        """
        alarmID = self.bCtrl.checkAlarms()
        if alarmID is not None: return ("Blender", alarmID, self.bCtrl.getAlarmDisplay(alarmID))
        alarmID = self.sCtrl.checkAlarms()
        if alarmID is not None: return ("Sensor", alarmID, self.sCtrl.getAlarmDisplay(alarmID))
        if self.upsAlarm:       return ("UPS", '1', self.constants.R_ALARMDISPLAY['1'])
        return None

    def muteAlarm(self, device, alarmID):
        """Ask the relevant deviceController to mute an alarm given by alarmID.
        """
        if device   == "Blender": self.bCtrl.muteAlarm(alarmID)
        elif device == "Sensor":  self.sCtrl.muteAlarm(alarmID)
        elif device == "UPS":
            self.upsAlarm = False
            self.aMute = int(time.time())
        else:
            print("Error: Device '%s' doesn't exist!" % (device))

    def showTime(self):
        """Display the current time in the view.
        """
        time = QTime.currentTime()
        text = time.toString("hh:mm:ss")
        self.vUI.ui.clk_label.display(text)
        if (time.second() % 2) == 0:
            text = text[:2] + ' ' + text[3:]

    def showStatus(self):
        """Display pixmaps and UPS-related stuff in the view.
        """
        try:
            # Access UPS data.
            data = subprocess.getoutput("apcaccess")
            bat_pos_i = data.find("BCHARGE")
            bat_pos_f = data.find("TIMELEFT")
            battery = data[(bat_pos_i+11):(bat_pos_f-11)]
            status_pos_i = data.find("STATUS")
            status_pos_f = data.find("LOADPCT")
            status = data[(status_pos_i+11):(status_pos_f-2)]

            # Check UPS status.
            if   (status == "ONLINE"): self.upsStatus = True  # UPS Connected.
            elif (status == "ONBATT"): self.upsStatus = False # UPS Disconnected.
            else:                      self.upsStatus = False # Any other message.

            # Update icons.
            self.vUI.ui.eng_but.setIcon(QIcon(":/img-logo"))
            self.vUI.ui.progressBar.setValue(int(battery))
        except ValueError as e: # Capture strange messages from UPS.
            self.upsStatus = False
            pass

    def upsCount(self):
        """
        Tick a counter if UPS is disconnected. If counter reaches UPSDISCONNECTTIME seconds, raise
        alarm.
        """
        if (self.upsStatus): self.upsCounter  = 0
        else:                self.upsCounter += 1
        if (self.upsCounter > self.constants.UPSDISCONNECTTIME
                and int(time.time()) - self.aMute > self.constants.ALARMMUTETIME):
            self.upsAlarm = True
        else:
            self.upsAlarm = False
