# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

import sys

import PySide2
from PySide2.QtWidgets import QApplication
import pyqtgraph as pg

from controller import Controller

if __name__ == "__main__":
    pg.setConfigOptions(antialias=True)

    PySide2.QtCore.QCoreApplication.setAttribute(PySide2.QtCore.Qt.AA_ShareOpenGLContexts)
    app = PySide2.QtWidgets.QApplication(sys.argv)

    widget = Controller.Controller()
    widget.show()
    PySide2.QtWidgets.QApplication.processEvents()
    app.aboutToQuit.connect(widget.closeForm)
    sys.exit(app.exec_())
