# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from state.DeviceState import DeviceState

class BlenderState(DeviceState):
    """Internal storage of the current state of the blender.
    """
    def __init__(self, constants):
        # JSON storage
        self.jsonDefFile = constants.B_DEFJSONPATH
        self.jsonFile    = constants.B_JSONPATH
        super(BlenderState, self).__init__(constants)

        # Load keys.
        self.cfgKeys  = constants.B_CFG
        self.aCfgKeys = constants.B_ALARM

        # Alarms setup.
        self.aDisplay = constants.B_ALARMDISPLAY
        self.aGen  = constants.B_ALARMGEN
        self.aMute = {key : 0 for key in self.aDisplay.keys()}

        # Measurements dictionary. Default values are 0.
        self.meass = {}
        for key in self.constants.B_MEASS:
            self.meass[key] = 0
