# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

import math, os

class Constants():
    """
    Location where all the program's constants are stored.

    A description of the parameter list that can be communicated to the arduinos can be found here:
    https://docs.google.com/spreadsheets/d/1HgJtS0TIsftUTY3CnlU96QEVCA-2ZOoImW1kF1uL5TE
    """

    add  = lambda a,b : math.floor(a+b)
    mult = lambda a,b : math.floor(a*b)

    # === PATHS ====================================================================================
    DUMPPATH      = os.path.dirname(os.path.abspath(__file__)) + "/../../dump/"
    B_DEFJSONPATH = DUMPPATH + "cfg_blender_default.json"
    B_JSONPATH    = DUMPPATH + "cfg_blender.json"
    S_DEFJSONPATH = DUMPPATH + "cfg_sensor_default.json"
    S_JSONPATH    = DUMPPATH + "cfg_sensor.json"
    H_JSONPATH    = DUMPPATH + "horometer.json"

    # === DEVICES ==================================================================================
    # All of these constants are arbitrary, and found experimentally.
    # NOTE: The values of LOOPRANGE and LOOPSPEED are very rough, and lack a precise calibration.
    BAUDRATE      = 115200 # Rate of information at which information is transfered in comm channel.
    TIMEOUT       = 1      # Time for timeout in seconds.
    ARDUINOVID    = 9025   # Arduino Vendor ID.
    LOOPRANGE     = 50     # Number of loops to wait for serial answer.
    LOOPSLEEP     = 0.02   # Amount of time between loops when waiting for serial answer.

    # === BLENDER ==================================================================================
    B_PID     = 67        # Arduino UNO Product ID.
    B_DEVNAME = "Blender" # Blender device name (as used in the code).
    B_OKPRFX  = "bk"      # Blender "ok" prefix, as received by the blender device.
    # Parameters
    B_SGNL  = ()                                         # Signals. Defined for consistency.
    B_CFG   = ('O','C','v',)                             # Configuration.
    B_CFGUI = ('O',)                                     # Configuration available to user.
    B_CTRL  = ('c','A','s','P','p','m','a','w','b','R',) # Control.
    B_ALARM = ('T','U','u',)                             # Alarms.
    B_ALARMGEN = {                                       # Default alarm value generator.
            'O' : ((mult, 0.80, 'u'), (mult, 1.20, 'U')),
    }

    # Blender measurements
    B_MEASS = {
            "mo" : 1, # measured oxygen filtered
    }

    # === SENSOR ===================================================================================
    S_PID     = 66       # Arduino MEGA Product ID.
    S_DEVNAME = "Sensor" # Sensor device name (as used in the code).
    S_OKPRFX  = "OK"     # Blender "ok" prefix, as received by the device.
    # Sensor parameters
    S_SGNL  = ('?', 'C')                                         # Request & Inspiratory pause.
    S_CFG   = ('M','E','V','F','R','p','P',)                     # Configuration.
    S_CFGUI = ('V','F','R','P','p',)                             # Configuration available to user.
    S_CTRL  = ('I','K','a','b','g','i','e','A','B','f','v','k',) # Control.
    S_ALARM = ('q','Q','w','W','u','U','o','O',)                 # Alarms.
    S_ALARMGEN = {                                               # Default alarm value generator.
            'F' : ((mult, 0.85, 'q'), (mult, 1.15, 'Q')),
            'V' : ((mult, 0.85, 'w'), (mult, 1.15, 'W')),
            'P' : ((mult, 0.85, 'u'), (mult, 1.15, 'U')),
            'p' : ((add, -2,    'o'), (add,  2,    'O')),
    }
    # Sensor measurements
    S_MEASS = {
            "nf" :  1, # net flow
            "nv" :  2, # net volume
            "tv" :  3, # tidal volume
            "pp" :  4, # pacient pressure
            "ap" :  5, # by air pressure
            "pe" :  6, # peep
            "rf" :  7, # respiratory frequency
            "mp" :  8, # maximum pressure
            "ti" :  9, # inspiration time
            "te" : 10, # expiration time
            "pl" : 11, # plateau pressure
    }

    # === ALARMS ===================================================================================
    UPSDISCONNECTTIME = 120 # Disconnection time (in seconds) after which an UPS disconnection
                            # alarm is thrown.
    ALARMMUTETIME = 120 # Effective time (in seconds) for the muting of an alarm.
    # Alarms priority and text. Priority is separated into three categories, high (1), medium (2),
    # and low (3).
    B_ALARMDISPLAY = {
            '1' : (1, "No se alcanza FiO2"),
            '3' : (1, "Caída de presión en estanque"),
            '4' : (1, "Sobre presion"),
            '5' : (1, "Desconexión línea de aire"),
            '6' : (1, "Desconexión línea de oxígeno"),
    }
    S_ALARMDISPLAY = {
            '1' : (1, "Obstrucción vía espiratoria"),
            '2' : (2, "No se alcanza frecuencia respiratoria"),
            '3' : (1, "Volumen por debajo del configurado"),
            '4' : (1, "No se alcanza volumen tidal"),
            '5' : (1, "Volumen por encima del configurado"),
            '6' : (1, "Se supera presión inspiratoria máxima"),
            '7' : (2, "No se alcanza PEEP"),
            '8' : (1, "Presión vía aérea cero"),
            '9' : (1, "Obstrucción vía inspiratoria"),
            'A' : (1, "PEEP sobre valor configurado"),
    }
    R_ALARMDISPLAY = {
            '1' : (2, "Modo bateria activado"),
    }
    # Buzzer rhythms.
    BUZZRHYTHMHIGH = [
            1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ]
    BUZZRHYTHMMED = [
            1, 1, 0, 0, 1, 1, 0, 0, 1, 1,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ]
    BUZZRHYTHMLOW = [
            1, 1,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ]

    def __init__(self):
        self.B_PARAMS  = self.B_CFG + self.B_CTRL + self.B_ALARM
        self.S_PARAMS  = self.S_CFG + self.S_CTRL + self.S_ALARM
