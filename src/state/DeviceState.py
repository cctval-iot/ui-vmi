# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from PySide2.QtCore import QObject
import json
import time # NOTE: This could also be implemented using QElapsedTimer.

class DeviceState(QObject):
    """
    Internal storage of the current state of the entire machine. Parent class to BlenderState and
    SensorState. Used to reduce requests to arduinos.
    """
    def __init__(self, constants):
        self.constants = constants
        self.signals   = None
        self.params    = None
        try:
            with open(self.jsonFile, 'r') as f:
                self.params = json.loads(f.read())
            return
        except FileNotFoundError as e:
            print("File '%s' not found! Defaulting..." % (self.jsonFile))
        except ValueError as e:
            print("JSONDecodeError: '%s'. Defaulting..." % (e))
        try:
            with open(self.jsonDefFile, 'r') as f:
                self.params = json.loads(f.read())
        except FileNotFoundError as e:
            print("Default file '%s' not found! Exiting..." % (self.jsonDefFile))
            exit(1)
        except ValueError as e:
            print("JSONDecodeError: '%s'. Exiting..." % (e))
            exit(1)

    def dump(self):
        """Save all saved parameters to the associated json file.
        """
        try:
            with open(self.jsonFile, 'w') as f:
                json.dump(self.params, f, skipkeys=False, ensure_ascii=False)
        except ValueError as e:
            print("Error while dumping parameters: '%s'. Skipping...")

    def setMeas(self, key, value):
        """
        Set a measurement to the internal state. SHOULD ONLY be called by `DeviceConnection` via
        `DeviceController`.
        """
        if key in self.meass:
            self.meass[key] = value
        else:
            print("ERROR: Key %s requested not in measurements!" % (key))

    def getMeas(self, key):
        """Get a currently set measurement from internal state.
        """
        if key in self.meass:
            return self.meass[key]
        else:
            print("ERROR: Key %s requested not in measurements!" % (key))
            return None

    def setParam(self, key, value):
        """
        Set a parameter in the internal state. If an alarm is associated to the parameter, it is
        automatically set as is defined by B_ALARMGEN and S_ALARMGEN in Constants.
        """
        if key in self.params:
            self.params[key] = value
            if key in self.aGen:
                for tup in self.aGen[key]:
                    self.setParam(tup[2], tup[0](tup[1], self.getParam(key)))
        else:
            print("ERROR: Key %s requested not in params!" % (key))
            return True
        return False

    def getParam(self, key):
        """Get a currently set parameter from internal state.
        """
        if key in self.params:
            return self.params[key]
        else:
            print("ERROR: Key '%s' requested not in params!" % (key))
            return None

    def getConfigParams(self):
        """Get the entire configuration saved in the internal state.
        """
        params = {}
        for cfgKey in self.cfgKeys:
            params[cfgKey] = self.params[cfgKey]
        return params

    def getAlarmConfigParams(self):
        """Get the entire alarm configuration saved in the internal state.
        """
        params = {}
        for aCfgKey in self.aCfgKeys:
            params[aCfgKey] = self.params[aCfgKey]
        return params

    def getAlarmDisplay(self, key):
        """Get alarm priority and description text based on id.
        """
        if key in self.aDisplay:
            return self.aDisplay[key]
        else:
            print("ERROR: Key '%s' requested not in alarm text dictionary!" % (key))
            return None

    def muteAlarm(self, id):
        """Mute an alarm for MUTETIME seconds.
        """
        self.aMute[id] = int(time.time())

    def checkAlarmMute(self, id):
        """Check if an alarm is muted.
        """
        if int(time.time()) - self.aMute[id] < self.constants.ALARMMUTETIME: return True
        else: return False
