# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from state.DeviceState import DeviceState

class SensorState(DeviceState):
    """Internal storage of the current state of the ventilator sensor.
    """
    def __init__(self, constants):
        # JSON storage.
        self.jsonDefFile = constants.S_DEFJSONPATH
        self.jsonFile    = constants.S_JSONPATH
        super(SensorState, self).__init__(constants)

        # Load keys.
        self.cfgKeys  = constants.S_CFG
        self.aCfgKeys = constants.S_ALARM

        # Alarms setup.
        self.aDisplay = constants.S_ALARMDISPLAY
        self.aGen  = constants.S_ALARMGEN
        self.aMute = {key : 0 for key in self.aDisplay.keys()}

        # Signals setup.
        self.signals = constants.S_SGNL

        # Measurements dictionary. Default values are 0.
        self.meass = {}
        for key in constants.S_MEASS.keys():
            self.meass[key] = 0
