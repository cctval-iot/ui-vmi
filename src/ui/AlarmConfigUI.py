# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from ui.UIElement import UIElement

class AlarmConfigUI(UIElement):
    """
    Handles the alarms configuration view. This includes:

    * Rendering the view.
    * Sending the alarms configuration data input by the user to the sensor arduino.
    """
    def __init__(self, uiController, constants):
        super(AlarmConfigUI, self).__init__()
        self.form = "alarmconfig.ui"
        self.setWindowTitle("Configuración de Alarmas")
        self.ui = self.loadUI()

        self.constants    = constants
        self.uiController = uiController

        # Associate each of the elements in the UI to the parameters in constants.S_ALARM.
        self.uiElems = {
                'q' : self.ui.f_resp_min,  'Q' : self.ui.f_resp_max,
                'w' : self.ui.v_tidal_min, 'W' : self.ui.v_tidal_max,
                'u' : self.ui.p_max_min,   'U' : self.ui.p_max_max,
                'o' : self.ui.peep_bajo,   'O' : self.ui.peep_sobre,
        }

        # Assure consistency between uiElems and constants.S_ALARM.
        for key in self.uiElems:
            if key not in self.constants.S_ALARM:
                print("FATAL ERROR: Logical inconsistency - not all uiElems in S_ALARM.")
                exit(1)
        for param in self.constants.S_ALARM:
            if param not in self.uiElems.keys():
                print("FATAL ERROR: Logical inconsistency - not all S_ALARM in uiElems.")
                exit(1)

        self.config = {} # Internal storage of the configuration input by the user.

    def readCfg(self):
        """Read and store current configuration from the internal state.
        """
        cfg = self.uiController.getAlarmConfigParams("Sensor")
        self.config = dict((p,cfg[p]) for p in self.constants.S_ALARM if p in cfg)

    def updateLabels(self):
        """Update labels in view.
        """
        for p in self.constants.S_ALARM:
            self.uiElems[p].setValue(self.config[p])

    def show(self):
        """
        Display the alarm configuration view, updating the labels in the view using the data from
        storage.
        """
        super().show()
        self.readCfg()
        self.updateLabels()

    def sendData(self):
        """Send configuration data from view to arduinos, then close the view.
        """
        self.readCfg()
        sendDict = {}
        for p in self.constants.S_ALARM:
            if self.config[p] != self.uiElems[p].value():
                sendDict[p] = self.uiElems[p].value()
                self.config[p] = self.uiElems[p].value()
        self.uiController.setParams("Sensor", sendDict)
        self.updateLabels()
        self.close()
