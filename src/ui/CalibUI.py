# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from ui.UIElement import UIElement

# NOTE: A lot of updates could be applied here to improve generality, similar to what was done for
#       AlarmConfigUI.py on commit beb97379. These should eventually be applied to improve the code.

class CalibUI(UIElement):
    """
    Handles the calibration view. This includes:

    * Rendering the view.
    * Accessing storage and getting the most recent calibration data.
    * Setting manual and deactivate control modes.
    * Sending the calibration data input by the user to the arduinos and storing it.
    """
    # NOTE: The calibration view is missing the 'g' parameter. This should be added before version
    #       2.0.
    def __init__(self, uiController, constants):
        # View initialization.
        super(CalibUI, self).__init__()
        self.form = "calib.ui"
        self.setWindowTitle("Calibración")
        self.ui = self.loadUI()

        # It's worth noting that not all control parameters from the constants are used in the
        # calibration view, so some of them are set here but are never used in the code.
        self.PARAMS = (constants.S_CTRL, constants.B_CTRL)
        self.uiController = uiController

        self.controlMode = False
        self.savedData = self.getData()
        self.ui.control_box.hide()

    def getData(self):
        """Get data from storage and store in an internal array of dictionaries.
        """
        data = [{},{}]
        for param in self.PARAMS[0]:
            data[0][param] = self.uiController.getParam("Sensor", param)
        for param in self.PARAMS[1]:
            data[1][param] = self.uiController.getParam("Blender", param)
        return data

    def show(self):
        """Load the saved data if available, and display the view.
        """
        # Sensor data
        if (self.savedData[0]):
            self.ui.cte_integral.setValue(self.savedData[0]["I"])
            self.ui.cte_proporcional.setValue(self.savedData[0]["K"])
            self.ui.alpha.setValue(self.savedData[0]["a"])
            self.ui.beta.setValue(self.savedData[0]["b"])
            self.ui.d_ins.setValue(self.savedData[0]["i"])
            self.ui.d_esp.setValue(self.savedData[0]["e"])
            self.ui.cte_k_vp1.setValue(self.savedData[0]["k"])

        # Blender data
        if (self.savedData[1]):
            self.ui.oxg.setValue(self.savedData[1]["c"])
            self.ui.pre_ope.setValue(self.savedData[1]["s"])
            self.ui.pre_max.setValue(self.savedData[1]["P"])
            self.ui.pre_min.setValue(self.savedData[1]["p"])
            self.ui.mix.setValue(self.savedData[1]["m"])
            self.ui.oxg_2.setValue(self.savedData[1]["A"])

        super().show()

    def deactivateControl(self): # NOTE: The way this is handled could be improved.
        """
        Set deactivate control mode to true or false. If to true, render the available saved data
        to the view.
        """
        self.controlMode = not self.controlMode
        if (not self.controlMode):
            self.ui.control_box.hide()
            self.uiController.setParams("Blender", {'C':1})
        else:
            # If 'a' is in the saved data we can safely assume that the rest of the deactivate
            # control data is there too.
            self.uiController.setParams("Blender", {'C':0})
            if ("a" in self.savedData[1]):
                if (self.savedData[1]["a"]) == 1: self.ui.vaire_on.setDown(True)
                else:                             self.ui.vaire_off.setDown(True)
                if (self.savedData[1]["w"]) == 1: self.ui.vo2_on.setDown(True)
                else:                             self.ui.vo2_off.setDown(True)
                if (self.savedData[1]["b"]) == 1: self.ui.byo2_on.setDown(True)
                else:                             self.ui.byo2_off.setDown(True)
                if (self.savedData[1]["R"]) == 1: self.ui.rel_on.setDown(True)
                else:                             self.ui.rel_off.setDown(True)
            self.ui.control_box.show()

    def adjustVP1(self):
        """Adjust the VP1 dial based on the value.
        """
        self.ui.vp1.setValue(self.ui.ctl_vp1.value())

    def adjustVF1(self):
        """Adjust the VF1 dial based on the value.
        """
        self.ui.vf1.setValue(self.ui.ctl_vf1.value())

    def update(self):
        """Update view based on timer.
        """
        self.updateTime()

    def updateTime(self):
        """Update horometer label.
        """
        self.ui.lab_horometro.setText(str(self.uiController.getCurrTime()))

    def setInitTime(self):
        """Set the horometer label on startup.
        """
        self.ui.lab_horometro.setText(str(self.uiController.getInitTime()))

    def sendData(self):
        """Send data to the arduino.
        """
        # Sensor Calibration
        sData = {}
        sData["I"] = self.ui.cte_integral.value()
        sData["K"] = self.ui.cte_proporcional.value()
        sData["a"] = self.ui.alpha.value()
        sData["b"] = self.ui.beta.value()
        sData["i"] = self.ui.d_ins.value()
        sData["e"] = self.ui.d_esp.value()
        sData["k"] = self.ui.cte_k_vp1.value()

        # Blender Calibration
        bData = {}
        bData["c"] = self.ui.oxg.value()
        bData["A"] = self.ui.oxg_2.value()
        bData["s"] = self.ui.pre_ope.value()
        bData["P"] = self.ui.pre_max.value()
        bData["p"] = self.ui.pre_min.value()
        bData["m"] = self.ui.mix.value()
        if (self.controlMode): # Only send this data if button is enabled.
            # If no button is pressed, assume False.
            if (self.ui.vaire_on.isChecked()): bData["a"] = 1
            else:                              bData["a"] = 0
            if (self.ui.vo2_on.isChecked()):   bData["w"] = 1
            else:                              bData["w"] = 0
            if (self.ui.byo2_on.isChecked()):  bData["b"] = 1
            else:                              bData["b"] = 0
            if (self.ui.rel_on.isChecked()):   bData["R"] = 1
            else:                              bData["R"] = 0

        sendDict = {}
        for key,value in sData.items():
            if sData[key] != self.savedData[0][key]:
                sendDict[key] = value
                self.savedData[0][key] = value
        self.uiController.setParams("Sensor", sendDict)

        sendDict = {}
        for key,value in bData.items():
            if bData[key] != self.savedData[1][key]:
                sendDict[key] = value
                self.savedData[1][key] = value
        self.uiController.setParams("Blender", sendDict)

        self.close()
