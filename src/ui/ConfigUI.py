# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from ui.UIElement import UIElement

class ConfigUI(UIElement):
    """
    Handles the configuration view. This includes:

    * Rendering the view.
    * Accessing storage to get the most recent configuration.
    * Sending the configuration data input by the user to the arduinos and storage.
    """
    def __init__(self, uiController, constants):
        super(ConfigUI, self).__init__()
        self.form = "config.ui"
        self.setWindowTitle("Configuración")
        self.ui = self.loadUI()

        self.uiController = uiController
        self.PARAMS       = (constants.S_CFGUI, constants.B_CFGUI)
        self.DEVICES      = ("Sensor", "Blender")

        # References to the view elements associated to each of the PARAMS.
        self.uiElems = (
                {'F' : self.ui.f_resp_spin,
                 'V' : self.ui.vol_spin,
                 'R' : self.ui.r_ins_spin,
                 'P' : self.ui.speep_spin,
                 'p' : self.ui.peep_spin},
                {'O' : self.ui.oxg_spin}
        )
        self.cfg = [{},{}] # Sensor and blender config.

        for i in range(len(self.DEVICES)):
            for key in self.uiElems[i]:
                if key not in self.PARAMS[i]:
                    print("FATAL ERROR: Logical inconsistency - not all uiElems in cfg constants!")
                    exit(1)
            for param in self.PARAMS[i]:
                if param not in self.uiElems[i].keys():
                    print("FATAL ERROR: Logical inconsistency - not all cfg constants in uiElems!")
                    exit(1)

    def readCfg(self):
        """Read sensor and blender configs and save them into internal state.
        """
        cfg = []
        for i in range(len(self.DEVICES)):
            cfg.append(self.uiController.getConfigParams(self.DEVICES[i]))
            self.cfg[i] = dict((k,cfg[i][k]) for k in self.PARAMS[i] if k in cfg[i])

    def updateLabels(self):
        """Update labels in view.
        """
        for i in range(len(self.DEVICES)):
            for param in self.PARAMS[i]:
                self.uiElems[i][param].setValue(self.cfg[i][param])

    def show(self):
        """
        Display the configuration view, updating the labels in the view using the data from storage.
        """
        super().show()
        self.readCfg()
        self.updateLabels()

    def updateCfg(self):
        """Update configuration based on data input by the user through the view.
        """
        self.readCfg() # Read the config in the view.
        for i in range(len(self.DEVICES)):
            sendDict = {}
            for param in self.PARAMS[i]:
                if self.cfg[i][param] != self.uiElems[i][param].value():
                    sendDict[param] = self.uiElems[i][param].value()
                    self.cfg[i][param] = self.uiElems[i][param].value()
            self.uiController.setParams(self.DEVICES[i], sendDict)
        self.close()

    def setStyleSheets(self):
        """Set the UI element's stylesheets.
        """
        self.ui.send_but.setStyleSheet(
                "QPushButton {" \
                    "background-color : rgb(0, 0, 127);" \
                    "border-style     : outset;" \
                    "color            : rgb(255, 255, 255);" \
                    "border-width     : 2px;" \
                    "border-radius    : 10px;" \
                    "padding          : 0px 10px 0px 10px;" \
                    "border-color     : beige;" \
                    "font             : 18pt Airbnb Cereal App Extra;}"
                "QPushButton:pressed {background-color: rgb(204, 0, 0)}"
        )
        self.ui.bck_but.setStyleSheet(
                "QPushButton {" \
                    "background-color : rgb(255, 0, 0);" \
                    "border-style     : outset;" \
                    "color            : rgb(255, 255, 255);" \
                    "border-width     : 2px;" \
                    "border-radius    : 10px;" \
                    "padding          : 0px 10px 0px 10px;" \
                    "border-color     : beige;" \
                    "font             : 16pt Airbnb Cereal App Extra;}"
                "QPushButton:pressed {background-color: rgb(0, 0, 127)}"
        )
