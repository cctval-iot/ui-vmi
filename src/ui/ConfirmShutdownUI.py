# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

import PySide2
from ui.UIElement import UIElement

class ConfirmShutdownUI(UIElement):
    """Small class to handle the shutdown confirmation view.
    """
    def __init__(self):
        super(ConfirmShutdownUI, self).__init__()
        self.form = "confirmshutdown.ui"
        self.setWindowTitle("Confirmar Detención")
        self.setWindowFlags(PySide2.QtCore.Qt.Tool)
        self.ui = self.loadUI()
