# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

import os
import collections
import numpy as np

from PySide2.QtCore import QFile, Qt
from PySide2.QtUiTools import QUiLoader
from pyqtgraph import PlotWidget
import pyqtgraph as pg
from ui.UIElement import UIElement

# NOTE: Both EngModeUI and VentUI should inherit from a "MainUI" class or something like that.
class EngModeUI(UIElement):
    """
    Handles the engineer mode view. This includes:

    * Rendering the view.
    * Receiving run data from devices.
    * Plotting the data received.
    """
    def __init__(self, uiController):
        # View initialization.
        super(EngModeUI, self).__init__()
        self.form = "engmode.ui"
        self.setWindowTitle("Calibración")
        self.ui = self.loadUI()

        self.uiController = uiController

        # Internal state.
        self.data   = []
        self.spam   = 100 # max size on plot
        self.pos    = 0
        self.nPlots = 6

        # References to view elements associated to the plots and labels.
        self.uiElems = (
                (self.ui.gra_spd1, self.ui.lab_spd1),
                (self.ui.gra_vol,  self.ui.lab_vol),
                (self.ui.gra_sp1,  self.ui.lab_sp1),
                (self.ui.gra_sp2,  self.ui.lab_sp2),
                (self.ui.gra_sf1,  self.ui.lab_sf1),
                (self.ui.gra_sf2,  self.ui.lab_sf2)
        )

        # Create and fill plots.
        for i in range(self.nPlots):
            self.data.append(collections.deque(np.zeros(self.spam), maxlen=self.spam))

        self.curveData = []
        for elem in self.uiElems:
            self.curveData.append(elem[0].plot(pen=pg.mkPen(color='w', width=3)))
            self.curveData[-1].getViewBox().setLimits(xMin=0, xMax=100)
            elem[0].setAxisItems(axisItems={'bottom':pg.AxisItem('bottom',showValues=False)})

    def loadUI(self):
        """Load the view itself.
        """
        loader = QUiLoader()
        ui_file = QFile(os.path.join(self.dir, self.form))
        ui_file.open(QFile.ReadOnly)
        loader.registerCustomWidget(PlotWidget)
        ui = loader.load(ui_file, self)
        ui_file.close()
        return ui

    def update(self):
        """Update labels in the view using the data received from the arduinos.
        """
        meass = (
                self.uiController.getMeas("Sensor", "nf"),
                self.uiController.getMeas("Sensor", "nv"),
                self.uiController.getMeas("Sensor", "tv"),
                self.uiController.getMeas("Sensor", "pp"),
                self.uiController.getMeas("Sensor", "ap"),
                self.uiController.getMeas("Sensor", "pe"),
        )

        hide = np.ones(self.spam)
        hide[self.pos:self.pos+1] = 0
        self.pos = (self.pos+1)%self.spam
        for i in range(self.nPlots):
            self.curveData[i].setData(self.data[i], connect=hide)
            self.data[i][self.pos%self.spam] = meass[i]
            self.uiElems[i][1].setText(str(meass[i]))
            ymin = np.min(self.data[i])
            ymax = np.max(self.data[i])
            self.curveData[i].getViewBox().setLimits(yMin=ymin,yMax=ymax)

    def resetPlots(self):
        """Reset the plots.
        """
        self.data = []
        self.pos = 0
        for i in range(self.nPlots):
            self.data.append(collections.deque(np.zeros(self.spam), maxlen=self.spam))

    def setStyleSheets(self):
        """Set the UI element's stylesheets.
        """
        self.ui.calib_but.setStyleSheet(
                "QPushButton {" \
                    "background-color : rgb(0, 0, 127);" \
                    "border-style     : outset;" \
                    "color            : rgb(255, 255, 255);" \
                    "border-width     : 2px;" \
                    "border-radius    : 10px;" \
                    "padding          : 0px 10px 0px 10px;" \
                    "border-color     : beige;" \
                    "font             : 15pt Airbnb Cereal App Extra;}"
                "QPushButton:pressed {background-color: rgb(8, 115, 255)}"
        )
