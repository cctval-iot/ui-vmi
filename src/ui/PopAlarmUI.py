# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

from PySide2.QtCore import QTimer, SIGNAL

try:
    import RPi.GPIO as GPIO
    RASP_OS = True
except (ImportError, RuntimeError):
    RASP_OS = False

import PySide2
from ui.UIElement import UIElement

class PopAlarmUI(UIElement):
    """
    Handles the popup alarm view. This includes:

    * Rendering the view.
    * Displaying the popup alarms.
    * Managing the silencing of alarms.
    """
    def __init__(self, uiController, constants):
        super(PopAlarmUI, self).__init__()
        self.form = "popalarm.ui"
        self.setWindowTitle("Popup")
        self.setWindowFlags(PySide2.QtCore.Qt.Tool)
        self.ui = self.loadUI()

        self.activeAlarm = None
        self.uiController = uiController

        # Buzzer setup.
        if RASP_OS:
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BOARD)
            GPIO.setup(7, GPIO.OUT)

            # Counter for the buzzer.
            self.buzzStep = 0
            self.rhythmHigh = constants.BUZZRHYTHMHIGH
            self.rhythmMed  = constants.BUZZRHYTHMMED
            self.rhythmLow  = constants.BUZZRHYTHMLOW

            # QTimer for playing the correct rhythm with the buzzer.
            buzzTimer = QTimer(self)
            self.connect(buzzTimer, SIGNAL("timeout()"), self.playRhythm)
            buzzTimer.start(100)

    def update(self):
        """Keep the alarm popup updated.
        """
        self.raiseAlarms()
        if self.activeAlarm != None: self.ui.raise_()

    def raiseAlarms(self):
        """Check if there are alarms up, creating the popup view if necessary.
        """
        alarm = self.uiController.checkAlarms()
        if alarm is None: return
        if self.activeAlarm is not None and alarm[2][0] >= self.activeAlarm[2][0]: return
        self.activeAlarm = alarm

        self.buzzStep = 0
        if   self.activeAlarm[2][0] == 1:
            self.ui.urgency_icon.setPixmap(":/img-alarm-high")
            if RASP_OS: self.buzzRhythm = self.rhythmHigh
        elif self.activeAlarm[2][0] == 2:
            self.ui.urgency_icon.setPixmap(":/img-alarm-medium")
            if RASP_OS: self.buzzRhythm = self.rhythmMed
        elif self.activeAlarm[2][0] == 3:
            self.ui.urgency_icon.setPixmap(":/img-alarm-low")
            if RASP_OS: self.buzzRhythm = self.rhythmLow
        else:
            print("Invalid alarm priority! Defaulting to high priority.")
            self.ui.urgency_icon.setPixmap(":/img-alarm-high")
            if RASP_OS: self.buzzRhythm = self.rhythmHigh

        self.ui.text.setText(self.activeAlarm[2][1])
        self.ui.move(-30,-30)
        self.ui.show()

    def mutePress(self):
        """Silence the alarm last displayed and close the alarm window.
        """
        self.uiController.muteAlarm(self.activeAlarm[0], self.activeAlarm[1])
        self.activeAlarm = None
        self.ui.close()

    def playRhythm(self):
        """Have the alarm play the appropiate rhythm depending on the alarm priority.
        """
        if self.activeAlarm != None:
            GPIO.output(7, self.buzzRhythm[self.buzzStep])
            self.buzzStep = (self.buzzStep+1) % len(self.buzzRhythm)
        else:
            GPIO.output(7, False) # Make sure that the buzzer is off.
