# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

import os
from PySide2.QtWidgets import QWidget
from PySide2.QtCore import QFile
from PySide2.QtUiTools import QUiLoader

class UIElement(QWidget):
    """Parent class to the other UI elements. Only handles the most basic functionalities.
    """
    def __init__(self):
        super(UIElement, self).__init__()
        self.dir = os.path.dirname(os.path.realpath(__file__))

    def loadUI(self):
        """Load the UI element.
        """
        ui_file = QFile(os.path.join(self.dir, self.form))
        ui_file.open(QFile.ReadOnly)
        ui = QUiLoader().load(ui_file, self)
        ui_file.close()
        return ui
