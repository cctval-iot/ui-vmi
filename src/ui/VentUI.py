# -*- coding: utf-8 -*-
# Copyright (C) CCTVal - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Bruno Benkel <bruno.benkel@usm.cl>, November 2020

import os
import collections
import numpy as np

from PySide2.QtCore import QFile, Qt
from PySide2.QtUiTools import QUiLoader
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
from ui.UIElement import UIElement

# NOTE: Both EngModeUI and VentUI should inherit from a "MainUI" class or something similar.
class VentUI(UIElement):
    """
    Handles the main view. This includes:

    * Rendering the view.
    * Displaying and updating use plots.
    """

    def __init__(self, uiController):
        super(VentUI, self).__init__()
        self.form = "vent.ui"
        self.setWindowTitle("VMI")
        self.ui = self.loadUI()

        self.uiController = uiController

        # Prepare internal state.
        self.uiPlots      = (self.ui.gra_vol, self.ui.gra_pres, self.ui.gra_f_neto)
        self.data         = []
        self.nPlots       = len(self.uiPlots) # Number of plots.
        self.compPlot     = self.ui.gra_comp
        self.compData     = []
        self.checkComp    = False             # Check the compliance for 2.5 seconds.
        self.compCount    = 0                 # Compliance calculating time counter.
        self.pSize        = 100               # Max size of plot.
        self.positionPlot = 0                 # Position in plots.

        # Setup plots.
        for i in range(self.nPlots):
            self.data.append(collections.deque(np.zeros(self.pSize), maxlen=self.pSize))
        for i in range(2):
            self.compData.append(collections.deque(np.zeros(self.pSize), maxlen=self.pSize))

        self.curveData = [
                self.uiPlots[0].plot(pen=pg.mkPen(color=(255,255,255), width=3), left=0, right=150),
                self.uiPlots[1].plot(pen=pg.mkPen(color=(255,  0,  0), width=3)),
                self.uiPlots[2].plot(pen=pg.mkPen(color=(  0,  0,255), width=3)),
                self.compPlot  .plot(pen=pg.mkPen(color=(255,255,  0), width=3)),
        ]

        # Infinite Line curve for update
        infLine = pg.InfiniteLine(pos=self.positionPlot, pen=pg.mkPen(color=(255,255,255), width=1,
                style=Qt.DashLine))
        self.lineCurve = [infLine for i in range(self.nPlots)]

        for i in range(self.nPlots):
            self.curveData[i].getViewBox().setLimits(xMin=0, xMax=100)
            self.curveData[i].getViewBox().addItem(self.lineCurve[i])
        self.curveData[0].getViewBox().setLimits(yMin = -3)
        self.curveData[1].getViewBox().setLimits(yMin = -3)

        for plot in self.uiPlots:
            plot.setAxisItems(axisItems={'bottom':pg.AxisItem('bottom', showValues=False)})

    def loadUI(self):
        """Load the UI element.
        """
        loader = QUiLoader()
        ui_file = QFile(os.path.join(self.dir, self.form))
        ui_file.open(QFile.ReadOnly)
        loader.registerCustomWidget(PlotWidget)
        ui = loader.load(ui_file, self)
        ui_file.close()
        return ui

    def setO2100(self):
        """Set O2 to 100%.
        """
        self.uiController.setParams("Blender", {'O' : 100})

    def askPlateauPressure(self):
        """Ask the sensor arduino the plateau pressure.
        """
        self.uiController.sendSignal("Sensor", 'C')
        self.checkComp = True
        self.compCount = 0

    def update(self):
        """Regular updates needed by the UI element.
        """
        self.updateLabels()
        self.updatePlots()
        self.updateO2Button()

    def updatePlots(self):
        """Maintain plots updated in real time with the internal state.
        """
        if self.uiController.isOn():
            self.data[0][self.positionPlot%self.pSize]     = self.uiController.getMeas("Sensor", "nv")
            self.data[1][self.positionPlot%self.pSize]     = self.uiController.getMeas("Sensor", "pp")
            self.data[2][self.positionPlot%self.pSize]     = self.uiController.getMeas("Sensor", "nf")
            self.compData[0][self.positionPlot%self.pSize] = self.uiController.getMeas("Sensor", "pp")
            self.compData[1][self.positionPlot%self.pSize] = self.uiController.getMeas("Sensor", "nv")

            hide = np.ones(self.pSize)
            hide[self.positionPlot:self.positionPlot+1] = 0
            self.positionPlot = (self.positionPlot+1)%self.pSize

            for i in range(self.nPlots):
                self.lineCurve[i].setValue((self.positionPlot-1 % self.pSize))
                self.curveData[i].setData(self.data[i], connect=hide)
            self.curveData[3].setData (self.compData[0], self.compData[1], connect=hide)

            self.curveData[0].getViewBox().setLimits(
                    yMax      = 1.5 * self.uiController.getParam("Sensor", 'V'),
                    minYRange = 1.5 * self.uiController.getParam("Sensor", 'V') + 3,
            )
            self.curveData[1].getViewBox().setLimits(
                    yMax      = 1.5 * self.uiController.getParam("Sensor", 'P'),
                    minYRange = 1.5 * self.uiController.getParam("Sensor", 'P') + 3,
            )
            self.curveData[2].getViewBox().setLimits(
                    yMin = np.min(self.data[2]),
                    yMax = np.max(self.data[2]),
            )
            self.curveData[3].getViewBox().setLimits(
                    xMin      = 0,
                    xMax      = 1.0 * self.uiController.getParam("Sensor", 'P'),
                    minXRange = 1.0 * self.uiController.getParam("Sensor", 'P') + 3,
                    yMin      = 0,
                    yMax      = 1.5 * self.uiController.getParam("Sensor", 'V'),
                    minYRange = 1.5 * self.uiController.getParam("Sensor", 'V') + 3,
            )

    def updateMeasurementsLabels(self):
        """Maintain the measurements labels updated with the internal state.
        """
        # General parameters.
        self.ui.lab_pre_via.setText(str(round(self.uiController.getMeas("Sensor",  "ap"), 1)))
        self.ui.lab_fre    .setText(str(int  (self.uiController.getMeas("Sensor",  "rf"))))
        self.ui.lab_volcorr.setText(str(int  (self.uiController.getMeas("Sensor",  "tv"))))

        self.ui.lab_pre_ins.setText(str(round(self.uiController.getMeas("Sensor",  "mp"), 1)))
        self.ui.lab_peep   .setText(str(round(self.uiController.getMeas("Sensor",  "pe"), 1)))
        self.ui.lab_o2     .setText(str(int  (self.uiController.getMeas("Blender", "mo"))))

        # IE.
        ti = self.uiController.getMeas("Sensor", "ti")
        te = self.uiController.getMeas("Sensor", "te")

        if ti <= te:
            i = 1
            e = te/ti if ti else 0.
        else:
            i = ti/te if te else 0.
            e = 1

        self.ui.lab_time_i.setText(str(round(ti, 1)))
        self.ui.lab_time_e.setText(str(round(te, 1)))
        self.ui.lab_relation_i.setText(str(round(i, 1)))
        self.ui.lab_relation_e.setText(str(round(e, 1)))

        # Compliance
        if self.checkComp:
            self.compCount += 1
            if self.compCount >= 25: self.checkComp = False

            tv   = self.uiController.getMeas("Sensor", "tv")
            pp   = self.uiController.getMeas("Sensor", "pl")
            peep = self.uiController.getMeas("Sensor", "pe")
            if abs(pp - peep) <= 0.1: self.ui.lab_comp.setText('-')
            else:                     self.ui.lab_comp.setText(str(round((tv / (pp - peep)), 1)))

    def updateParameterLabels(self):
        """Maintain the configuration parameters labels updated with the internal state.
        """
        self.ui.lab_frec_conf.setText(str(self.uiController.getParam("Sensor",  'F')))
        self.ui.lab_vol_conf .setText(str(self.uiController.getParam("Sensor",  'V')))
        self.ui.lab_peep_conf.setText(str(self.uiController.getParam("Sensor",  'p')))
        self.ui.lab_oxg_conf .setText(str(self.uiController.getParam("Blender", 'O')))
        self.ui.lab_fre_min .setText(str(int(self.uiController.getParam("Sensor",  'q'))))
        self.ui.lab_fre_max .setText(str(int(self.uiController.getParam("Sensor",  'Q'))))
        self.ui.lab_vol_min .setText(str(int(self.uiController.getParam("Sensor",  'w'))))
        self.ui.lab_vol_max .setText(str(int(self.uiController.getParam("Sensor",  'W'))))
        self.ui.lab_peep_min.setText(str(int(self.uiController.getParam("Sensor",  'o'))))
        self.ui.lab_peep_max.setText(str(int(self.uiController.getParam("Sensor",  'O'))))
        self.ui.lab_oxg_min .setText(str(int(self.uiController.getParam("Blender", 'u'))))
        self.ui.lab_oxg_max .setText(str(int(self.uiController.getParam("Blender", 'U'))))

    def updateLabels(self):
        """Maintain the labels updated with the internal state.
        """
        if self.uiController.isOn():
            self.updateMeasurementsLabels()
        self.updateParameterLabels()

    def updateO2Button(self):
        """
        Set the background color of the O2 100% button depending on the internal state of the
        o2100state variable.
        """
        if self.uiController.getParam("Blender", 'O') == 100:
            self.ui.mod3_but.setStyleSheet(
                    "background-color: rgb(78, 154, 6);\n" \
                    "border-style: outset;\n" \
                    "color : rgb(255, 255, 255);\n" \
                    "border-width: 2px;\n" \
                    "border-radius: 10px;\n" \
                    "padding: 0px 10px 0px 10px;\n" \
                    "border-color: beige;\n" \
                    "font:18pt 'Airbnb Cereal App Extra';\n"
            )
        else:
            self.ui.mod3_but.setStyleSheet(
                    "background-color: rgb(0, 0, 127);\n" \
                    "border-style: outset;\n" \
                    "color : rgb(255, 255, 255);\n" \
                    "border-width: 2px;\n" \
                    "border-radius: 10px;\n" \
                    "padding: 0px 10px 0px 10px;\n" \
                    "border-color: beige;\n" \
                    "font:18pt 'Airbnb Cereal App Extra';\n"
            )

    def resetPlots(self):
        """Empty and reset the plots in the view.
        """
        # NOTE: Currently, this doesn't actually empty the plots. It would be ideal if it did, but
        #       since it's not an essential feature it's left aside for the time being.
        self.data = []
        self.positionPlot = 0
        for i in range(self.nPlots):
            self.data.append(collections.deque(np.zeros(self.pSize), maxlen = self.pSize))

    def emptyLabels(self):
        """Empty the parameters labels in the view.
        """
        self.ui.lab_pre_via.setText("")
        self.ui.lab_fre    .setText("")
        self.ui.lab_volcorr.setText("")
        self.ui.lab_pre_ins.setText("")
        self.ui.lab_peep   .setText("")
        self.ui.lab_o2     .setText("")

    def setStyleSheets(self):
        """Set the stylesheets necessary for the UI element.
        """
        self.ui.mod2_but.setStyleSheet(
                "QPushButton {" \
                    "background-color : rgb(78, 154, 6);" \
                    "border-style     : outset;" \
                    "color            : rgb(255, 255, 255);" \
                    "border-width     : 2px;" \
                    "border-radius    : 10px;" \
                    "padding          : 0px 10px 0px 10px;" \
                    "border-color     : beige;" \
                    "font             : 18pt Airbnb Cereal App Extra;}" \
                "QPushButton:pressed {background-color: rgf(204, 0, 0)}"
        )
        self.ui.mod3_but_2.setStyleSheet(
                "QPushButton {" \
                    "background-color : rgb(0, 0, 127);" \
                    "border-style     : outset;" \
                    "color            : rgb(255, 255, 255);"\
                    "border-width     : 2px;" \
                    "border-radius    : 10px;" \
                    "padding          : 0px 1px 0px 1px;" \
                    "border-color     : beige;" \
                    "font             : 16pt Airbnb Cereal App Extra;}"
                "QPushButton:pressed {background-color: rgb(8, 115, 255)}"
        )
        self.ui.config_but.setStyleSheet(
                "QPushButton {" \
                    "background-color : rgb(0, 0, 127);" \
                    "border-style     : outset;" \
                    "color            : rgb(255, 255, 255);"\
                    "border-width     : 2px;" \
                    "border-radius    : 10px;" \
                    "padding          : 0px 1px 0px 1px;" \
                    "border-color     : beige;" \
                    "font             : 13pt Airbnb Cereal App Extra;}"
                "QPushButton:pressed {background-color: rgb(8, 115, 255)}"
        )
        self.ui.mod3_but.setStyleSheet(
                "QPushButton {" \
                    "background-color : rgb(0, 0, 127);" \
                    "border-style     : outset;" \
                    "color            : rgb(255, 255, 255);" \
                    "border-width     : 2px;" \
                    "border-radius    : 10px;" \
                    "padding          : 0px 10px 0px 10px;" \
                    "border-color     : beige;" \
                    "font             : 18pt Airbnb Cereal App Extra;}"
                "QPushButton:pressed {background-color: rgb(8, 115, 255)}"
        )
        self.ui.but_ala.setStyleSheet(
                "QPushButton {" \
                    "background-color : rgb(0, 0, 127);" \
                    "border-style     : outset;" \
                    "color            : rgb(255, 255, 255);" \
                    "border-width     : 2px;" \
                    "border-radius    : 10px;" \
                    "padding          : 0px 10px 0px 10px;" \
                    "border-color     : beige;" \
                    "font             : 18pt Airbnb Cereal App Extra;}"
                "QPushButton:pressed {background-color: rgb(8, 115, 255)}"
        )
